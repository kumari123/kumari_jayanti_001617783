/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Prashant
 */
public class VitalSign {
    private String patientName; 
    private int patientId;
    private int patientAge;
   
    private float respiratoryRate;
    private float heartRate;
    private float SystolicBloodPressure;
    private float weight;
   
     private static int count=0;
    
    public VitalSign()
    {
        
        patientId=count;
        ++count;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int PatientAge) {
        this.patientAge = patientAge;
    }
 
    
    
    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
   
  
 public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
    
    public float getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(float respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public float getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }

    public float getSystolicBloodPressure() {
        return SystolicBloodPressure;
    }

    public void setSystolicBloodPressure(float SystolicBloodPressure) {
        this.SystolicBloodPressure = SystolicBloodPressure;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return  patientName ;
    }
    
   

   
     
    
}
