/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Child.Child;
import Business.Child.ChildDirectory;
import Business.Child.Disability;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class AssignmentWorkRequest extends WorkRequest{
    private String assignment;
    private String report;
    private Child child;

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }
    
 
    
    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }
    
    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

   


   
    
}
