/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Child.ChildDirectory;
import Business.Doctor.DoctorDirectory;
import Business.Donor.DonorDirectory;
import Business.Employee.EmployeeDirectory;
import Business.Parent.ParentDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public abstract class Organization {
    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private ParentDirectory parentDirectory;
    private DonorDirectory donorDirectory;
    private DoctorDirectory doctorDirectory;
    private ChildDirectory childDirectory;
    private int organizationID;
    private static int counter;
    
    public enum Type{
        Admin("Admin Organization"),ConfirmDoctor("ConfirmDoctor Organization"), SchoolDoctor("SchoolDoctor Organization"),
        Teacher("Teacher Organization"),CareGiver("Caregiver Organization"),Receptionist("Receptionist Organization"),
        Parent("Parent Organization"),ConfirmAdmin("ConfirmAdmin"),BankPerson("BankPerson");;
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        parentDirectory=new ParentDirectory();
        doctorDirectory=new DoctorDirectory();
        donorDirectory=new DonorDirectory();
        childDirectory=new ChildDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public ParentDirectory getParentDirectory() {
        return parentDirectory;
    }

    public DonorDirectory getDonorDirectory() {
        return donorDirectory;
    }

    public DoctorDirectory getDoctorDirectory() {
        return doctorDirectory;
    }

    public ChildDirectory getChildDirectory() {
        return childDirectory;
    }
    

    @Override
    public String toString() {
        return name;
    }
    
    
}
