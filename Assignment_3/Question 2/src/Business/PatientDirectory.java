/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class PatientDirectory {
    private ArrayList<Patient> patientList;

    public PatientDirectory()
    {
       this.patientList=new ArrayList<>();
    }
    
    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<Patient> patientList) {
        this.patientList = patientList;
    }
    
     public Patient addPatient()
    {
        Patient patient =new Patient();
        this.patientList.add(patient);
        return patient;
    }
    
    public void deletePatient(Patient patient)
    {
        patientList.remove(patient);
    }
    
     public Patient searchPatient(String id)
    {
        for(Patient pa:this.patientList)
        {
           if(pa.getId().equals(id))
           {
               return pa;
           }
        }
        return null;
    }
}
