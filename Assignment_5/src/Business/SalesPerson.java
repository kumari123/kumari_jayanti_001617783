/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Prashant
 */
public class SalesPerson {
    private String salesPersonName;
    private double commision;

    public double getCommision() {
        return commision;
    }

    public void setCommision(double commision) {
        this.commision = commision;
    }

    
    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    @Override
    public String toString() {
        return salesPersonName; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
