/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Child;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Prashant
 */
public class ChildDirectory {
    private int age; 
    private String address;
    private String disability;
    private Date date;
    private Child child;
    private ArrayList<Child> childlist;
    
    
   
    
    
    public ChildDirectory()
    {
        childlist=new ArrayList<Child>();
       
    } 
   

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    

    public ArrayList<Child> getChildlist() {
        return childlist;
    }

    public void setChildlist(ArrayList<Child> childlist) {
        this.childlist = childlist;
    }

    
    public Child addChildren(){
        Child c =new Child();
        childlist.add(c);
        return c;
       }
    
    public void deleteChildren(Child ch){
        childlist.remove(ch);
    }
    
    public Child searchChild(int id){
        for(Child c:childlist)
        {
            if(c.getChildId()==id)
                return c;
        }
        return null;
    }
      
     public boolean checkIfChiIdUserNameIsUnique(String childUserId)
    {
        for (Child ch : childlist)
        {
            if (ch.getChildUserId().equals(childUserId))
                return false;
        }
        return true;
    }
            
}
