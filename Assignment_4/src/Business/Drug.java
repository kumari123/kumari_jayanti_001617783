/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class Drug {
    
    private String drugName;
    private String manufactureDate;
    private String expiryDate;
    private int serialNo;
     
    private int Availability;
    private String manufacturerName;
    private Float drugPrice;
    
 //   private static int count =0;

    @Override
    public String toString() {
        return drugName; //To change body of generated methods, choose Tools | Templates.
    }

    
   /* public Drug() {
    count++;
    serialNo = count;
    }
*/
    public Float getDrugPrice() {
        return drugPrice;
    }

    public void setDrugPrice(Float drugPrice) {
        this.drugPrice = drugPrice;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
    

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(int serialNo) {
        this.serialNo = serialNo;
    }

    public int getAvailability() {
        return Availability;
    }

    public void setAvailability(int Availability) {
        this.Availability = Availability;
    }

}
