/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.BankRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class BankOrganization extends Organization{

    
    public BankOrganization()
    {
        super(Organization.Type.BankPerson.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles= new ArrayList<>();
        roles.add(new BankRole());
        return roles;
    }
    
}
