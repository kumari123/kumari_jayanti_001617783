/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Parent;

import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class ParentDirectory {
    private ArrayList<Parent> parentList;
    
    public ParentDirectory()
    {
        parentList=new ArrayList<>();
    }

    public ArrayList<Parent> getParentList() {
        return parentList;
    }

    public void setParentList(ArrayList<Parent> parentList) {
        this.parentList = parentList;
    }
    
    
}
