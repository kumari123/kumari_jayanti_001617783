/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prashant
 */
public class ProductCatalog {
     private List<Product> productCatalog;

    public ProductCatalog() {
    productCatalog = new ArrayList<Product>();
    }
    
    
    public List<Product> getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(List<Product> productCatalog) {
        this.productCatalog = productCatalog;
    }
    
    
    
    public Product addProduct(){
        Product p = new Product();
        productCatalog.add(p);
        return p;
    }
    
    public void removeProduct(Product p){
        productCatalog.remove(p);
    }
    
    public Product searchProduct(String nm){
        for (Product product : productCatalog) {
            if(product.getProdName()==nm){
                return product;
            }
        }
        return null;
    }
    
}
