/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Patient;
import Business.Person;
import Business.VitalSign;
import Business.VitalSignHistory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Prashant
 */
public class SearchPersonResultJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SearchPersonResultJPanel
     */
    
    private Person person;
    JPanel userProcessContainer;
    private VitalSign vitalSign;
    private VitalSignHistory vitalSignList;
    public SearchPersonResultJPanel(JPanel upc,Person p) {
        initComponents();
       this.person=p;
        this.userProcessContainer=upc;
       this.vitalSign= vitalSign;
        populateForm();
    }

   public void populateForm()
   {
       personNamejTextField.setText(person.getPersonName());
       personAgejTextField.setText(String.valueOf(person.getPersonAge()));
       personAddressjTextField.setText(person.getAddress());
       
       respRatejTextField.setText(String.valueOf(vitalSign.getRespiratoryRate()));
       hrtRatejTextField.setText(String.valueOf(vitalSign.getHeartRate()));
       sysbpjTextField.setText(String.valueOf(vitalSign.getSystolicBloodPressure()));
       wtjTextField.setText(String.valueOf(vitalSign.getWeight()));

   }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        respRatejTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        wtjTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        hrtRatejTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        sysbpjTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        personNamejTextField = new javax.swing.JTextField();
        personAgejTextField = new javax.swing.JTextField();
        personAddressjTextField = new javax.swing.JTextField();
        backjButton = new javax.swing.JButton();

        jLabel1.setText("Name");

        jLabel2.setText("Heart Rate");

        jLabel3.setText("Respiratory Rate");

        jLabel4.setText("Systolic Blood Pressure");

        jLabel5.setText("Weight");

        jLabel6.setText("Address");

        jLabel7.setText("Age");

        backjButton.setText("BACK");
        backjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(backjButton)
                .addContainerGap(540, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(137, 137, 137)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5)
                        .addComponent(jLabel4)
                        .addComponent(jLabel3)
                        .addComponent(jLabel1)
                        .addComponent(jLabel7)
                        .addComponent(jLabel6)
                        .addComponent(jLabel2))
                    .addGap(155, 155, 155)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(personNamejTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                        .addComponent(personAgejTextField)
                        .addComponent(personAddressjTextField)
                        .addComponent(hrtRatejTextField)
                        .addComponent(respRatejTextField)
                        .addComponent(sysbpjTextField)
                        .addComponent(wtjTextField))
                    .addContainerGap(138, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(460, Short.MAX_VALUE)
                .addComponent(backjButton)
                .addGap(45, 45, 45))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(129, 129, 129)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(personNamejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(personAgejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(22, 22, 22)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(personAddressjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(25, 25, 25)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(hrtRatejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(22, 22, 22)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(respRatejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(17, 17, 17)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(sysbpjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(25, 25, 25)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(wtjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(130, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backjButtonActionPerformed
       userProcessContainer.remove(this);             //this represents the current upc needs to be removed,removes the topmost panel
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer); // TODO add your handling code here:
    }//GEN-LAST:event_backjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JTextField hrtRatejTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField personAddressjTextField;
    private javax.swing.JTextField personAgejTextField;
    private javax.swing.JTextField personNamejTextField;
    private javax.swing.JTextField respRatejTextField;
    private javax.swing.JTextField sysbpjTextField;
    private javax.swing.JTextField wtjTextField;
    // End of variables declaration//GEN-END:variables
}
