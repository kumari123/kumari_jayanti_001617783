/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


import static com.sun.deploy.uitoolkit.impl.fx.DeployPerfLogger.timestamp;
import static java.sql.JDBCType.TIMESTAMP;
import static java.sql.Types.TIMESTAMP;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static oracle.jrockit.jfr.events.ContentTypeImpl.TIMESTAMP;

/**
 *
 * @author Prashant
 */
public class VitalSign

{
    private float respiratoryRate;
    private float heartRate;
    private float SystolicBloodPressure;
    private float weight;
    private String tstmp;
    java.util.Date date = new java.util.Date();

   /* public Date getCurrentTimestamp() {
    Calendar calendar = Calendar.getInstance();
    java.util.Date now = calendar.getTime();
    java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
        return currentTimestamp;
    }

    public void setCurrentTimestamp(Date currentTimestamp) {
       Calendar calendar = Calendar.getInstance();
    java.util.Date now = calendar.getTime();
    java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime()); 
       this.currentTimestamp = currentTimestamp;
    }
    */

    public float getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(float respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public float getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }

    public float getSystolicBloodPressure() {
        return SystolicBloodPressure;
    }

    public void setSystolicBloodPressure(float SystolicBloodPressure) {
        this.SystolicBloodPressure = SystolicBloodPressure;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    
   

   @Override
    public String toString() {
        return  " " + date ;
        
    }
/*
    //public String getTstmp() {
     //   SimpleDateFormat t= new SimpleDateFormat("yyyy/mm/dd  HH:mm:ss");
      //  return t.format(tstmp);
        
   // }

   // public void setTstmp(String tstmp) throws ParseException {
        //this.tstmp = tstmp;
    //   SimpleDateFormat t= new SimpleDateFormat("yyyy/mm/dd  HH:mm:ss");
      //  this.tstmp = t.parse(tstmp);
   // }

    
    
 */
    
    
}
    
