/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Patient;
import Business.PatientDirectory;
import Business.Person;
import Business.VitalSign;
import Business.VitalSignHistory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Prashant
 */
public class ManagePatientJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TablePatientJPanel
     */
      private Patient patient;
    private VitalSignHistory vitalSignList;
    private PatientDirectory patientList;
    JPanel userProcessContainer;
    public ManagePatientJPanel(JPanel upc,VitalSignHistory vSignList) {
        initComponents();
        this.vitalSignList=vSignList;
        this.userProcessContainer=upc;
        populateTable();
    }

     public void populateTable()
    {
        DefaultTableModel dtm=(DefaultTableModel) PatientjTable.getModel();
        
        dtm.setRowCount(0);
       
        for(VitalSign vs:vitalSignList.getVitalSignList())
        {
            Object row[]=new Object[2];
            row[0]=vs;
            if(!vitalSignStatus(vs))
            {
                row[1]="Abnormal";
            }else{
                row[1]="Normal";
            }
            dtm.addRow(row);
        }
        
        
    }
     
       private boolean vitalSignStatus(VitalSign vs)
 {
       
        if(patient.getAge()<=3&&patient.getAge()>=1)
        {
           
        if ( (vs.getRespiratoryRate()>=20&& vs.getRespiratoryRate()<=30)&&
                (vs.getHeartRate() >= 80 && vs.getHeartRate() <= 130)&&
                (vs.getSystolicBloodPressure() >=80&&vs.getSystolicBloodPressure() <= 110)&&
                (vs.getWeight()>= 22 && vs.getWeight() <=31))
        
            return true;
            
            
        }
        else if(patient.getAge()<=5&&patient.getAge()>=4)
        {
            if( (vs.getHeartRate()>=80 && vs.getHeartRate()<=120)&&
               ( vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30)&&
                (vs.getSystolicBloodPressure()>=80 && vs.getSystolicBloodPressure()<=110)&&
                (vs.getWeight()>=31 && vs.getWeight()<=40))
             return true;
            
        }
        else if(patient.getAge()<=12&&patient.getAge()>=6)
        {
            if((vs.getHeartRate()>=70 && vs.getHeartRate()<=100)&&
                (vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30)&&
                (vs.getSystolicBloodPressure()>=80 && vs.getSystolicBloodPressure()<=120)&&
                (vs.getWeight()>=41 && vs.getWeight()<=92))
             return true;
            
        }
        else if(patient.getAge()>=13) 
                {
             if((vs.getHeartRate()>=55 && vs.getHeartRate()<=105)&&
                vs.getRespiratoryRate()>=12 && vs.getRespiratoryRate()<=20&&
                vs.getSystolicBloodPressure()>=110 && vs.getSystolicBloodPressure()<=120&&
                (vs.getWeight()>100 )) 
                return true;
             
                 }
      return false;            
 }  
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        PatientjTable = new javax.swing.JTable();
        searchjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        viewjButton = new javax.swing.JButton();
        searchPatjTextField = new javax.swing.JTextField();
        deletePatientjButton = new javax.swing.JButton();

        PatientjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Respiratory Rate", "VitalSign Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(PatientjTable);
        if (PatientjTable.getColumnModel().getColumnCount() > 0) {
            PatientjTable.getColumnModel().getColumn(0).setResizable(false);
            PatientjTable.getColumnModel().getColumn(1).setResizable(false);
        }

        searchjButton.setText("SEARCH USING PATIENT ID");
        searchjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchjButtonActionPerformed(evt);
            }
        });

        backjButton.setText("BACK");

        viewjButton.setText("VIEW DETAILS");
        viewjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewjButtonActionPerformed(evt);
            }
        });

        deletePatientjButton.setText("DELETE");
        deletePatientjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePatientjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(backjButton)))
                .addContainerGap(201, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchjButton)
                    .addComponent(viewjButton))
                .addGap(109, 109, 109)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(deletePatientjButton)
                    .addComponent(searchPatjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(75, 75, 75))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 193, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchjButton)
                    .addComponent(searchPatjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewjButton)
                    .addComponent(deletePatientjButton))
                .addGap(64, 64, 64)
                .addComponent(backjButton)
                .addGap(89, 89, 89))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void deletePatientjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePatientjButtonActionPerformed
      int selectedRow=PatientjTable.getSelectedRow();
        if(selectedRow>=0)
        {
            Patient pa = (Patient) PatientjTable.getValueAt(selectedRow, 0);
            patientList.deletePatient(pa);
            JOptionPane.showMessageDialog(this, "Patient has been deleted","Information",JOptionPane.INFORMATION_MESSAGE);
            populateTable();
        }
        else 
        {
            JOptionPane.showMessageDialog(this, "Please select an Account from the table","Error",JOptionPane.INFORMATION_MESSAGE);
        }        
// TODO add your handling code here:
    }//GEN-LAST:event_deletePatientjButtonActionPerformed

    private void searchjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchjButtonActionPerformed
       String id=searchPatjTextField.getText();
           
           Patient p=patientList.searchPatient(id);
           if(p==null)
       {
         //display a msg to the user that acc was not found 
           JOptionPane.showMessageDialog(this, "Patient not found","Information",JOptionPane.INFORMATION_MESSAGE);
           return;
       }else{
           //display the details of the acc in another panel (search results)
           SearchPatientResultJPanel sp=new SearchPatientResultJPanel(userProcessContainer,p);
           userProcessContainer.add(sp);
           CardLayout layout=(CardLayout) userProcessContainer.getLayout();
           layout.next(userProcessContainer);
        // TODO add your handling code here:
    }//GEN-LAST:event_searchjButtonActionPerformed
  }
  
    private void viewjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewjButtonActionPerformed
     
        int selectedRow=PatientjTable.getSelectedRow();
         if(selectedRow >= 0)
        {
            //get the selected obj
            Patient p = (Patient) PatientjTable.getValueAt(selectedRow, 0);
            
           ViewPatientJPanel va= new ViewPatientJPanel(userProcessContainer,p,this); //this is obj of manageaccountJpanel
           userProcessContainer.add("ViewPatientJPanel",va);
           CardLayout layout=(CardLayout) userProcessContainer.getLayout();
           layout.next(userProcessContainer);
        }
        else 
        {
            JOptionPane.showMessageDialog(this, "Please select an Account from the table","Error",JOptionPane.INFORMATION_MESSAGE);
        }
  
        // TODO add your handling code here:
    }//GEN-LAST:event_viewjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable PatientjTable;
    private javax.swing.JButton backjButton;
    private javax.swing.JButton deletePatientjButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField searchPatjTextField;
    private javax.swing.JButton searchjButton;
    private javax.swing.JButton viewjButton;
    // End of variables declaration//GEN-END:variables
}

