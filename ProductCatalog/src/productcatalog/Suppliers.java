/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productcatalog;

/**
 *
 * @author Prashant
 */
public class Suppliers {
    private String supplierName;
    private String supplierId;
    private int supplierPrice;

     private ProductCatalog productcatalog;
    
    public Suppliers()
    {
     productcatalog= new ProductCatalog(); 
    }

    public ProductCatalog getProductcatalog() {
        return productcatalog;
    }

    public void setProductcatalog(ProductCatalog productcatalog) {
        this.productcatalog = productcatalog;
    }
    
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public int getSupplierPrice() {
        return supplierPrice;
    }

    public void setSupplierPrice(int supplierPrice) {
        this.supplierPrice = supplierPrice;
    }
    
   
    
    //String[] sup = {"Dell","Lenovo","HP","Apple","Toshiba"};
    //System.out.println("List of Suppliers:" +sup);
}
