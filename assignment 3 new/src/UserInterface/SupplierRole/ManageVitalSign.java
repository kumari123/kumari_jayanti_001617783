package UserInterface.SupplierRole;

import Business.Person;
import Business.PersonDirectory;
import Business.VitalSign;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Rushabh
 */
public class ManageVitalSign extends javax.swing.JPanel {

    
  
    /** Creates new form ManageProductCatalogJPanel */
    JPanel userProcessContainer;
    private Person person;
    private PersonDirectory personList;
     private VitalSign vitalSign;
    
    public ManageVitalSign(JPanel upc,Person person,VitalSign vitalSign) {
        initComponents();
      //this.personList=personList;
     // this.person=person;
        this.vitalSign=vitalSign;
        this.userProcessContainer=upc;
        this.person=person;
        
        txtName.setText(person.getPersonName());
        populateTable();
    }
    
     public void populateTable()
    {
        DefaultTableModel model =  (DefaultTableModel) manageTable.getModel();
        model.setRowCount(0);
       
        for(VitalSign vs:person.getVitalSignHistory().getVitalSignList())
        {
            Object row[]=new Object[8];
            row[0]=vs;
            row[1]=vs.getPatientId();
            
            row[2]=vs.getRespiratoryRate();
            row[3]= vs.getHeartRate();
            row[4]=vs.getSystolicBloodPressure();
            row[5]=vs.getWeight();
            row[6]=vitalSignStatus(vs);
           /* if(!vitalSignStatus(vs))
            {
                row[6]="normal";
            }else{
                row[6]="ABNormal";
            }
                   */
            row[7]=vs.getPatientAge();
                   
            model.addRow(row);
        }
    }
     
     private String vitalSignStatus(VitalSign vs)
 {
      
       String Status = "Abnormal";
       System.out.println(Status);
       
        if(vs.getPatientAge()<=3 && vs.getPatientAge()>=0)
        {
           System.out.println(Status);
        if ( (vs.getRespiratoryRate()>=20&& vs.getRespiratoryRate()<=30)&&
                (vs.getHeartRate() >= 80 && vs.getHeartRate() <= 130)&&
                (vs.getSystolicBloodPressure() >=80&&vs.getSystolicBloodPressure() <= 110)&&
                (vs.getWeight()>= 22 && vs.getWeight() <=31))
        
            
        {Status= "Normal";
        System.out.println(Status);
        }
            
        }
        else if(vs.getPatientAge()<=5 && vs.getPatientAge()>=4)
        {
            if( (vs.getHeartRate()>=80 && vs.getHeartRate()<=120)&&
               ( vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30)&&
                (vs.getSystolicBloodPressure()>=80 && vs.getSystolicBloodPressure()<=110)&&
                (vs.getWeight()>=31 && vs.getWeight()<=40))
             
            {  Status= "Normal";}
        }
        else if(vs.getPatientAge()<=12 && vs.getPatientAge()>=6)
        {
            if((vs.getHeartRate()>=70 && vs.getHeartRate()<=100)&&
                (vs.getRespiratoryRate()>=20 && vs.getRespiratoryRate()<=30)&&
                (vs.getSystolicBloodPressure()>=80 && vs.getSystolicBloodPressure()<=120)&&
                (vs.getWeight()>=41 && vs.getWeight()<=92))
                System.out.println(Status);
            {  Status=  "Normal";}
            
        }
        else if(vs.getPatientAge()>=13) 
                {
             if((vs.getHeartRate()>=55 && vs.getHeartRate()<=105)&&
                vs.getRespiratoryRate()>=12 && vs.getRespiratoryRate()<=20&&
                vs.getSystolicBloodPressure()>=110 && vs.getSystolicBloodPressure()<=120&&
                (vs.getWeight()>100 )) 
             {  Status= "Normal";}
             
                 }
 
      return Status;            
 } 

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        manageTable = new javax.swing.JTable();
        btnView = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        btnDelete = new javax.swing.JButton();
        chartjButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 102, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("MANAGE VITAL SIGN");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, -1, -1));

        manageTable.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        manageTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Person Name", "Person ID", "Respiratory Rate", "Heart Rate", "Sysbolic Blood Pressure", "Weight", "VitalSign Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(manageTable);
        if (manageTable.getColumnModel().getColumnCount() > 0) {
            manageTable.getColumnModel().getColumn(0).setResizable(false);
            manageTable.getColumnModel().getColumn(1).setResizable(false);
            manageTable.getColumnModel().getColumn(2).setResizable(false);
            manageTable.getColumnModel().getColumn(3).setResizable(false);
            manageTable.getColumnModel().getColumn(4).setResizable(false);
            manageTable.getColumnModel().getColumn(5).setResizable(false);
            manageTable.getColumnModel().getColumn(6).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 920, 130));

        btnView.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnView.setText("View Vital Sign>>");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 500, 180, -1));

        btnCreate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCreate.setText("Create Vital Sign >>");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        add(btnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 500, -1, -1));

        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setText("Search >>");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 20, 130, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 110, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Person");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, 170, -1));

        txtName.setEditable(false);
        txtName.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        txtName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(102, 102, 102), null, null));
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, 150, 30));

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDelete.setText("Delete Person(s)");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 500, 170, -1));

        chartjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        chartjButton.setText("Chart");
        chartjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chartjButtonActionPerformed(evt);
            }
        });
        add(chartjButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 110, 140, 30));
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
      int row= manageTable.getSelectedRow();
         
         if(row<0)
         {
             JOptionPane.showMessageDialog(this, "please select any row","Warning",JOptionPane.WARNING_MESSAGE);
         }
         else
         {
             VitalSign v=(VitalSign) manageTable.getValueAt(row,0);
             
             ViewVitalSign vsjp=new ViewVitalSign(userProcessContainer,v,this);
             userProcessContainer.add("ViewVitalSign",vsjp);
             CardLayout layout=(CardLayout) userProcessContainer.getLayout();
             layout.next(userProcessContainer);
         }
        
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        CreateVitalSignJPanel mswa=new CreateVitalSignJPanel(userProcessContainer,person.getVitalSignHistory());
       userProcessContainer.add("CreateVitalSignJPanel",mswa);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);  
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
         SearchVitalSign vsjp=new SearchVitalSign(userProcessContainer,person.getVitalSignHistory());
             userProcessContainer.add("SearchVitalSign",vsjp);
             CardLayout layout=(CardLayout) userProcessContainer.getLayout();
             layout.next(userProcessContainer);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
       userProcessContainer.remove(this);
       CardLayout layout =(CardLayout)userProcessContainer.getLayout();
       layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
       int row= manageTable.getSelectedRow();
         
         if(row<0)
         {
             JOptionPane.showMessageDialog(this, "please select any row","Warning",JOptionPane.WARNING_MESSAGE);
         }
         else
         {
             VitalSign p=(VitalSign) manageTable.getValueAt(row,0);
             //personList.deletePerson(person);
             
             populateTable();
         }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void chartjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chartjButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chartjButtonActionPerformed

 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnView;
    private javax.swing.JButton chartjButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable manageTable;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
