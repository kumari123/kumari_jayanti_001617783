package UserInterface.StoreRole;

import Business.Drug;
import Business.Order;
import Business.OrderCatalog;
import Business.OrderItem;
import Business.Supplier;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Rushabh
 */
public class ManageDrugCatalogJPanel extends javax.swing.JPanel {

    
  
    /** Creates new form ManageDrugCatalogJPanel */
    private JPanel userProcessContainer;
    private Supplier supplier;
     private OrderCatalog orderCatalog;
    private Order order;
    public ManageDrugCatalogJPanel(JPanel upc, Supplier s,OrderCatalog orderCatalog) {
        initComponents();
        userProcessContainer = upc;
        supplier = s;
        this.orderCatalog = orderCatalog;
        this.order = new Order();
        txtName.setText(s.getSupplierName());
        refreshTable();
    }

     public void refreshTable() {
        int rowCount = drugCatalogTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel)drugCatalogTable.getModel();
        for(int i=rowCount-1;i>=0;i--) {
            model.removeRow(i);
        }
        
        for(Drug d : supplier.getDrugCatalog().getDrugCatalog()) {
            Object row[] = new Object[7];
            row[0] = d;
            row[1] = d.getManufactureDate();
            row[2] = d.getExpiryDate();
            row[3] = d.getSerialNo();
            row[4] = d.getAvailability();
            if(d.getAvailability()<10)
            {
                JOptionPane.showMessageDialog(null, "Threshold reached.Please fill Quantity", "Warning", JOptionPane.WARNING_MESSAGE);
            }
            row[5] = d.getManufacturerName();
            row[6] = d.getDrugPrice();
              model.addRow(row);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugCatalogTable = new javax.swing.JTable();
        btnView = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        btnDelete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        itemjTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        salesPricejTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        QuantityjSpinner = new javax.swing.JSpinner();
        addToCartjButton = new javax.swing.JButton();
        removeItemjButton = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Manage Drug Catalog");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        drugCatalogTable.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        drugCatalogTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Drug Name", "Manufacture date", "Expiry Date", "Serial No.", "Availability", "Manufacturer Name", "Drug Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(drugCatalogTable);
        if (drugCatalogTable.getColumnModel().getColumnCount() > 0) {
            drugCatalogTable.getColumnModel().getColumn(0).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(1).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(2).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(3).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(4).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(5).setResizable(false);
            drugCatalogTable.getColumnModel().getColumn(6).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 870, 110));

        btnView.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnView.setText("View Drug Detail >>");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 260, 220, -1));

        btnCreate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCreate.setText("Create New Drug >>");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        add(btnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 260, -1, -1));

        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setText("Search >>");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 20, 130, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 600, 110, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Supplier:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, 170, -1));

        txtName.setEditable(false);
        txtName.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        txtName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(102, 102, 102), null, null));
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 150, 30));

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDelete.setText("Delete Drug(s)");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 260, 190, -1));

        itemjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Drug Name", "Sales Price", "Quantity", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(itemjTable);
        if (itemjTable.getColumnModel().getColumnCount() > 0) {
            itemjTable.getColumnModel().getColumn(0).setResizable(false);
            itemjTable.getColumnModel().getColumn(1).setResizable(false);
            itemjTable.getColumnModel().getColumn(2).setResizable(false);
            itemjTable.getColumnModel().getColumn(3).setResizable(false);
        }

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, -1, 100));

        jLabel3.setText("Item in Cart");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 330, 80, 20));

        jLabel4.setText("Quantity");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 450, -1, -1));
        add(salesPricejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 390, 150, -1));

        jLabel5.setText("Sales Price");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 390, -1, -1));
        add(QuantityjSpinner, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 450, -1, -1));

        addToCartjButton.setText("Add to Cart");
        addToCartjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addToCartjButtonActionPerformed(evt);
            }
        });
        add(addToCartjButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 450, -1, -1));

        removeItemjButton.setText("Remove Item");
        removeItemjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeItemjButtonActionPerformed(evt);
            }
        });
        add(removeItemjButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 520, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed

        int row = drugCatalogTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Drug p = (Drug)drugCatalogTable.getValueAt(row, 0);
        ViewDrugDetailJPanel vpdjp = new ViewDrugDetailJPanel(userProcessContainer, p);
        userProcessContainer.add("ViewProductDetailJPanel", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
       
        CreateNewDrugJPanel cnpjp = new CreateNewDrugJPanel(userProcessContainer, supplier);
        userProcessContainer.add("CreateNewDrugJPanel", cnpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed

        SearchForDrugJPanel sfpjp = new SearchForDrugJPanel(userProcessContainer, supplier);
        userProcessContainer.add("SearchForDrugJPanel", sfpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        int row = drugCatalogTable.getSelectedRow();
        
        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Drug s = (Drug)drugCatalogTable.getValueAt(row, 0);
        supplier.getDrugCatalog().removeDrug(s);
        refreshTable();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void addToCartjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addToCartjButtonActionPerformed
       int row = drugCatalogTable.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(this, "Select any row", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        //check if the sales price has been entered
        Drug selectDrug = (Drug)drugCatalogTable.getValueAt(row, 0);

        int salesPrice = 0;
        try {
            salesPrice = Integer.parseInt(salesPricejTextField.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "provide integer value", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

     //salesprice should be greater product price
        if (salesPrice < selectDrug.getDrugPrice()) {
            JOptionPane.showMessageDialog(this, "SAles price should be greate than product price", "warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

     //quantity should be greater than 0 and less than availability
        int fetchQuantity = (Integer) QuantityjSpinner.getValue();
        if (fetchQuantity <= 0) {
            JOptionPane.showMessageDialog(this, "Quantity should be greater than 0 ", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        if (fetchQuantity > selectDrug.getAvailability()) {
            JOptionPane.showMessageDialog(this, "Quantity should be less than availability", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
       
        
       
        
       // System.out.print("adding" + order.getOrderItemList().size());

        boolean alreadyPresent = false;
        //checking if product is already in orderitem list
        for (OrderItem item : order.getOrderItemList()) {
            if (item.getDrug().equals(selectDrug)) {
                alreadyPresent = true;
                int oldAvail = selectDrug.getAvailability();
                int newAvail = oldAvail - fetchQuantity;
                selectDrug.setAvailability(newAvail);
                item.setQuantity(fetchQuantity + item.getQuantity());
                refreshTable();
                populateCartTable();
                break;
            }
        }

        //adding new product to orderitem list
        if (!alreadyPresent) {
           // System.out.print("existing");
            int oldAvail = selectDrug.getAvailability();
            int newAvail = oldAvail - fetchQuantity;
            selectDrug.setAvailability(newAvail);

            order.addOrderItem(fetchQuantity, salesPrice, selectDrug);
            refreshTable();
            populateCartTable();
        }
    }
// TODO add your handling code here:

    public void populateCartTable() {
        DefaultTableModel model = (DefaultTableModel) itemjTable.getModel();
        model.setRowCount(0);

        for (OrderItem item : order.getOrderItemList()) {
            Object row[] = new Object[4];
            row[0] = item;
            row[1] = item.getSalesPrice();
            row[2] = item.getQuantity();
            row[3] = item.getSalesPrice() * item.getQuantity();
            
            model.addRow(row);
        }

      
        
// TODO add your handling code here:
    }//GEN-LAST:event_addToCartjButtonActionPerformed

    private void removeItemjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeItemjButtonActionPerformed
        int rowCount = itemjTable.getSelectedRowCount();
        if (rowCount <= 0) {
            JOptionPane.showMessageDialog(this, "select any row", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            OrderItem item = (OrderItem) itemjTable.getValueAt(rowCount, 0);
            Drug selectedProduct = item.getDrug();
            int oldAvail = selectedProduct.getAvailability();
            int newAvail = oldAvail + item.getQuantity();
            selectedProduct.setAvailability(newAvail);

            order.removeOrderItem(item);
            JOptionPane.showMessageDialog(this, "ITEM has been deleted", "information", JOptionPane.INFORMATION_MESSAGE);

            refreshTable();
            populateCartTable();
        }      
        
// TODO add your handling code here:
    }//GEN-LAST:event_removeItemjButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner QuantityjSpinner;
    private javax.swing.JButton addToCartjButton;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnView;
    private javax.swing.JTable drugCatalogTable;
    private javax.swing.JTable itemjTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton removeItemjButton;
    private javax.swing.JTextField salesPricejTextField;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
