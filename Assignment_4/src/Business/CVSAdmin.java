/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Prashant
 */
public class CVSAdmin {
    private StoreCatalog storeCatalog;
    private SupplierDirectory suppDirectory;
    private OrderCatalog orderCatalog;
    
    public CVSAdmin()
    {    
        orderCatalog =new OrderCatalog();
        storeCatalog =new StoreCatalog();
        suppDirectory =new SupplierDirectory();
    }

    public OrderCatalog getOrderCatalog() {
        return orderCatalog;
    }

    public void setOrderCatalog(OrderCatalog orderCatalog) {
        this.orderCatalog = orderCatalog;
    }

    
    public StoreCatalog getStoreCatalog() {
        return storeCatalog;
    }

    public void setStoreCatalog(StoreCatalog storeCatalog) {
        this.storeCatalog = storeCatalog;
    }

    public SupplierDirectory getSuppDirectory() {
        return suppDirectory;
    }

    public void setSuppDirectory(SupplierDirectory suppDirectory) {
        this.suppDirectory = suppDirectory;
    }
    
    
}
