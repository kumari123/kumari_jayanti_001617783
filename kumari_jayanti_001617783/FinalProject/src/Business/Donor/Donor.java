/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

/**
 *
 * @author Prashant
 */
public class Donor {
    private int donorId;
    private String donorName;
    private int donationAmt;

    public int getDonorId() {
        return donorId;
    }

    public void setDonorId(int donorId) {
        this.donorId = donorId;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public int getDonationAmt() {
        return donationAmt;
    }

    public void setDonationAmt(int donationAmt) {
        this.donationAmt = donationAmt;
    }
    
    public String toString()
    {
        return donorName;
    }
    
}
