/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.ConfirmDoctorRole;
import Business.Role.Role;
import Business.Role.SchoolDoctorRole;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class ConfirmSchoolDocOrganization extends Organization{
    
     public ConfirmSchoolDocOrganization()
    {
        super(Organization.Type.ConfirmDoctor.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        
        ArrayList<Role> roles=new ArrayList<>();
        roles.add(new ConfirmDoctorRole());
        return roles;
    }
    }
    

