/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class OrganizationDirectory {
     private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type)
    {
        Organization organization=null;
        if(type.getValue().equals(Type.CareGiver.getValue())){
            organization=new CaregiverOrganization();
            organizationList.add(organization);
        }
       
       else if(type.getValue().equals(Type.Parent.getValue())){
                organization=new ParentOrganization();
                organizationList.add(organization);
                }
       else if(type.getValue().equals(Type.Receptionist.getValue())){
            organization=new ReceptionistOrganization();
            organizationList.add(organization);
        }
       else if(type.getValue().equals(Type.SchoolDoctor.getValue())){
            organization=new SchoolDoctorOrganization();
            organizationList.add(organization);
        }
       else if(type.getValue().equals(Type.Teacher.getValue())){
            organization=new TeacherOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.ConfirmDoctor.getValue())){
            organization=new ConfirmSchoolDocOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.ConfirmAdmin.getValue())){
            organization=new ConfirmAdminOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.BankPerson.getValue())){
            organization=new BankOrganization();
            organizationList.add(organization);
        }
        return  organization;
          
    }
}

