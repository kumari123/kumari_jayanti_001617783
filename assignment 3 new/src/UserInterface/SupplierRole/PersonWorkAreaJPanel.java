package UserInterface.SupplierRole;

import Business.Person;
import Business.PersonDirectory;
import Business.VitalSign;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class PersonWorkAreaJPanel extends javax.swing.JPanel {

    JPanel userProcessContainer;
    private Person person;
    private VitalSign vitalSign;
    public PersonWorkAreaJPanel(JPanel upc,Person person,VitalSign vitalSign) {
        initComponents();
        userProcessContainer=upc;
        this.vitalSign=vitalSign;
        this.person=person;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        managePersonButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 102, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("My Work Area (Product Manager Role)");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, -1, -1));

        managePersonButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        managePersonButton.setText("Manage Person Directory >>");
        managePersonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                managePersonButtonActionPerformed(evt);
            }
        });
        add(managePersonButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void managePersonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_managePersonButtonActionPerformed
       ManageVitalSign mswa=new ManageVitalSign(userProcessContainer,person,vitalSign);
       userProcessContainer.add("ManageVitalSign",mswa);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);   
    }//GEN-LAST:event_managePersonButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton managePersonButton;
    // End of variables declaration//GEN-END:variables
}
