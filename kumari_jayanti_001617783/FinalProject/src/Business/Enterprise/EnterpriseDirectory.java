/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Child.ChildDirectory;
import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class EnterpriseDirectory {
     private ArrayList<Enterprise> enterpriseList;
     private ChildDirectory childDir;
    
    public EnterpriseDirectory()
    {
        enterpriseList=new ArrayList<>();
        childDir=new ChildDirectory();
    }

    public ChildDirectory getChildDir() {
        return childDir;
    }

    public void setChildDir(ChildDirectory childDir) {
        this.childDir = childDir;
    }

    
    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public Enterprise addEnterprise(String name,Enterprise.EnterpriseType type)
    {
        Enterprise ent= null;
        if(type ==Enterprise.EnterpriseType.Hospital)
        {
            ent=new HospitalEnterprise(name);
            enterpriseList.add(ent);
        }
        else if(type==Enterprise.EnterpriseType.School)
        {
            ent=new SchoolEnterprise(name);
            enterpriseList.add(ent);
        }
        else if(type==Enterprise.EnterpriseType.Bank)
        {
            ent=new BankEnterprise(name);
            enterpriseList.add(ent);
        }
        return ent;
    }
    
}
