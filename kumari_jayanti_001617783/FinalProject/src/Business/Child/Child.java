/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Child;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Prashant
 */
public class Child {
   // private int age;
    private String name;
    private String motherName;
    private String fatherName;
    private String age;
    private String address;
    private String childUserId;
    private Date admissionDate;
    private int childId;
  
   
    private static int count=1;
    private ArrayList<ChildDirectory> childDetails;
     private ArrayList<Disability> disabilitiList;

    
    public Child()
    {
        admissionDate=new Date();
        childDetails=new ArrayList<ChildDirectory>();
        childId=count;
        count++;
     }
     public ArrayList<Disability> getDisabilitiList() {
        return disabilitiList;
    }

    public void setDisabilitiList(ArrayList<Disability> disabilitiList) {
        this.disabilitiList = disabilitiList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
     public Disability addDisability(){
        Disability d =new Disability();
        disabilitiList.add(d);
        return d;
       }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
     

    public String getChildUserId() {
        return childUserId;
    }

    public void setChildUserId(String childUserId) {
        this.childUserId = childUserId;
    }
    

    public ArrayList<ChildDirectory> getChildDetails() {
        return childDetails;
    }

    public void setChildDetails(ArrayList<ChildDirectory> childDetails) {
        this.childDetails = childDetails;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }
    

   
     public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
    
    public String toString()
    {
        return childId +"";
        
    }
    
    public ChildDirectory addChildDetails(){
        ChildDirectory chlist = new ChildDirectory();
        childDetails.add(chlist);
        return chlist;
    }
    
    
}
