package UserInterface.StoreRole;

import Business.OrderCatalog;
import Business.Store;
import Business.StoreCatalog;
import Business.Supplier;
import Business.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class LoginStore extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
   // private SupplierDirectory supplierDirectory;
    private StoreCatalog storeCatalog;
    private OrderCatalog orderCatalog;
    public LoginStore(JPanel upc,StoreCatalog storeCatalog,OrderCatalog orderCatalog) {
        initComponents();
        userProcessContainer = upc;
        //supplierDirectory = sd;
        this.storeCatalog = storeCatalog;
        this.orderCatalog= orderCatalog;
        
        storecombobox.removeAllItems();;
        for (Store store : storeCatalog.getStoreList()) {
            storecombobox.addItem(store);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        sNmaeTextField1 = new javax.swing.JTextField();
        btnFind = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        storecombobox = new javax.swing.JComboBox();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Store Location");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, -1, 30));

        sNmaeTextField1.setEditable(false);
        sNmaeTextField1.setBorder(null);
        add(sNmaeTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 190, 150, 30));

        btnFind.setText("GO>>");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });
        add(btnFind, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 130, -1, 30));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("Store Login");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, -1, -1));

        storecombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        storecombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storecomboboxActionPerformed(evt);
            }
        });
        add(storecombobox, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 130, 150, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed

        Store store  = (Store) storecombobox.getSelectedItem();
        ManageSupplierCatalog swajp = new ManageSupplierCatalog(userProcessContainer, store);
        userProcessContainer.add("ManageSupplierCatalog", swajp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnFindActionPerformed

    private void storecomboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storecomboboxActionPerformed

    }//GEN-LAST:event_storecomboboxActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFind;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField sNmaeTextField1;
    private javax.swing.JComboBox storecombobox;
    // End of variables declaration//GEN-END:variables
    
}
