/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class DrugCatalog {
    
    private List<Drug> drugCatalog;

    public DrugCatalog() {
    drugCatalog = new ArrayList<Drug>();
    }

    public List<Drug> getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(List<Drug> drugCatalog) {
        this.drugCatalog = drugCatalog;
    }
    
     public Drug addDrug(){
        Drug p = new Drug();
        drugCatalog.add(p);
        return p;
    }
    
    public void removeDrug(Drug d){
        drugCatalog.remove(d);
    }
    
    public Drug searchDrug(String nm){
        for (Drug drug : drugCatalog) {
            if(drug.getDrugName()==nm){
                return drug;
            }
        }
        return null;
    }
}
