/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Doctor;

import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class DoctorDirectory {
    private ArrayList<Doctor> doctorlist;
    
    public DoctorDirectory(){
        doctorlist=new ArrayList<>();
    }

    public ArrayList<Doctor> getDoctorlist() {
        return doctorlist;
    }
    
    public Doctor addDoctor(){
        Doctor d= new Doctor();
        doctorlist.add(d);
        return d;
    }
    
    public void deleteDoctor(Doctor d){
        doctorlist.remove(d);
    }
    
    public Doctor searchDoctor(int id){
        for(Doctor d:doctorlist)
        {
            if(d.getDocId()==id)
                return d;
        }
        return  null;
    }
}
