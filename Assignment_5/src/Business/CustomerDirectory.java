/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prashant
 */
public class CustomerDirectory {
    private List<Customer> customerList;
    public CustomerDirectory() {
    
        customerList = new ArrayList<Customer>();
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }
    
    
   
   
    
    public Customer addCustomer(){
        Customer s = new Customer();
        customerList.add(s);
        return s;
    }
    
    public void removeCustomer(Customer c){
        customerList.remove(c);
    }
    
    public Customer searchCustomer(String keyword){
        for (Customer customer : customerList) {
            if(customer.getCustomerName().equals(keyword)){
                return customer;
            }
        }
        return null;
    }  
}
