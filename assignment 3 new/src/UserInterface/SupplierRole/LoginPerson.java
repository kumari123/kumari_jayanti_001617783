package UserInterface.SupplierRole;

import Business.Person;
import Business.PersonDirectory;
import Business.VitalSign;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class LoginPerson extends javax.swing.JPanel {
     private JPanel userProcessContainer;
    private PersonDirectory personDir;
    private VitalSign vitalSign;
    public LoginPerson(JPanel upc,PersonDirectory personDir,VitalSign vitalSign) {
        initComponents();
        userProcessContainer=upc;
        this.personDir=personDir;
        this.vitalSign=vitalSign;
        
        personComboBox.removeAllItems();
        for(Person p:personDir.getPersonList())
                personComboBox.addItem(p);
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnFind = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        personComboBox = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 102, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Person Name");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, -1, 30));

        btnFind.setText("GO>>");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });
        add(btnFind, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 130, -1, 30));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("PERSON LOGIN");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 50, -1, -1));

        personComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        personComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                personComboBoxActionPerformed(evt);
            }
        });
        add(personComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 130, 150, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed
       Person person= (Person) personComboBox.getSelectedItem();
       PersonWorkAreaJPanel awjp=new PersonWorkAreaJPanel(userProcessContainer,person,vitalSign);
       userProcessContainer.add("SupplierWorkAreaJPanel",awjp);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnFindActionPerformed

    private void personComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_personComboBoxActionPerformed

    }//GEN-LAST:event_personComboBoxActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFind;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JComboBox personComboBox;
    // End of variables declaration//GEN-END:variables
    
}
