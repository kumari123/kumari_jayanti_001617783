package UserInterface.SupplierRole;

import Business.VitalSign;
import Business.VitalSignHistory;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class CreateVitalSignJPanel extends javax.swing.JPanel {
 private JPanel userProcessContainer;
    private VitalSignHistory vitalSignList;
    public CreateVitalSignJPanel(JPanel upc,VitalSignHistory vsList){
        initComponents();
        this.userProcessContainer=upc;
        this.vitalSignList=vsList;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtheart = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        respRatetxt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        sysbpjTextField = new javax.swing.JTextField();
        wtjTextField = new javax.swing.JTextField();
        patientNamejTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        agejTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 102, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("CREATE VITAL SIGN");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 30, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Respiratory Rate");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 180, 30));

        txtId.setEditable(false);
        txtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 90, 210, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Heart Rate");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 110, 30));

        txtheart.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtheart, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 290, 210, 30));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setText("Add Vital Sign");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 470, -1, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Patient Name");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, -1, 30));

        respRatetxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(respRatetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 240, 210, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Weight");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("Sysbolic Blood Pressure");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, -1, -1));
        add(sysbpjTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, 210, 30));
        add(wtjTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 390, 210, 30));
        add(patientNamejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, 210, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Patient Id");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("Patient age");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, -1, -1));

        agejTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agejTextFieldActionPerformed(evt);
            }
        });
        add(agejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 190, 210, 30));
    }// </editor-fold>//GEN-END:initComponents
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
      VitalSign v=vitalSignList.addVitalSign();
      v.setPatientName(patientNamejTextField.getText());
      v.setPatientAge(Integer.parseInt(agejTextField.getText()));
      v.setRespiratoryRate(Float.parseFloat(respRatetxt.getText()));
      v.setHeartRate(Float.parseFloat(txtheart.getText()));
      v.setSystolicBloodPressure(Float.parseFloat(sysbpjTextField.getText()));
      v.setWeight(Float.parseFloat(wtjTextField.getText()));
      
     JOptionPane.showMessageDialog(this, "VitalSign has been added successfully","Information",JOptionPane.INFORMATION_MESSAGE);
    
}//GEN-LAST:event_btnAddActionPerformed

  
    
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        backAction();
    }//GEN-LAST:event_btnBackActionPerformed

    private void agejTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agejTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_agejTextFieldActionPerformed
   
      public void backAction()
   {
       userProcessContainer.remove(this);
       Component [] componentArray=userProcessContainer.getComponents();
       Component component=componentArray[componentArray.length -1];
       ManageVitalSign manageVS = (ManageVitalSign) component;//component array is of component type and we need this to bcome
                                                                 //of type managesupplier as we have to refresh table present in managesupplier
       
        manageVS.populateTable();
       
       CardLayout layout= (CardLayout) userProcessContainer.getLayout();
       layout.previous(userProcessContainer);
   
    }   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField agejTextField;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField patientNamejTextField;
    private javax.swing.JTextField respRatetxt;
    private javax.swing.JTextField sysbpjTextField;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtheart;
    private javax.swing.JTextField wtjTextField;
    // End of variables declaration//GEN-END:variables
}
