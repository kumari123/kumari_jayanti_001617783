/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class Order {
    private ArrayList<OrderItem> orderItemList;
    private int orderNumber;
    private static int count=0;
    
    public Order()
    {
        count++;
        orderNumber=count;
        orderItemList=new ArrayList<OrderItem>();
        
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
    
    public void removeOrderItem(OrderItem item)
    {
        orderItemList.remove(item);
    }
    
    public OrderItem addOrderItem(int quantity,int price,Product p)   //passing the object so that while traversing ordered items are not lost
    {
        OrderItem item=new OrderItem();
        item.setProduct(p);
        item.setQuantity(quantity);
        item.setPrice(price);
        
        orderItemList.add(item);
        return item;
    }
    
    
    
}
