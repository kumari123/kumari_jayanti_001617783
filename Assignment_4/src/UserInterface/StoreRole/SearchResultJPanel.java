package UserInterface.StoreRole;

import Business.Drug;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class SearchResultJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Drug drug;
    public SearchResultJPanel(JPanel upc, Drug d) {
        initComponents();
        userProcessContainer = upc;
        drug = d;
        txtName.setText(d.getDrugName());
        manufactureDate.setText(d.getManufactureDate());
        expiryDatetxt.setText(d.getExpiryDate());
        serialNotxt.setText(String.valueOf(d.getSerialNo()));
     
        availabilityjTextField1.setText(String.valueOf(d.getAvailability()));
        manufacturerjTextField1.setText(d.getManufacturerName());
        drugPricejTextField.setText(String.valueOf(d.getDrugPrice()));
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        expiryDatetxt = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        manufactureDate = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        serialNotxt = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        drugPricejTextField = new javax.swing.JTextField();
        availabilityjTextField1 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        manufacturerjTextField1 = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("View Drug Detail");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(135, 25, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Drug Name:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 30));

        txtName.setEditable(false);
        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 90, 159, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Expiry Date:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 30));

        expiryDatetxt.setEditable(false);
        expiryDatetxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        expiryDatetxt.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(expiryDatetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 170, 159, -1));

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setText("Update Product");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 530, 176, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 540, -1, -1));

        manufactureDate.setEditable(false);
        manufactureDate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        manufactureDate.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        add(manufactureDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 130, 159, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Manufacture Date:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, 30));

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setText("SAVE");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 540, 70, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Serial No:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));
        add(serialNotxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, 160, -1));

        jLabel6.setText("Manufacturer Name:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 350, -1, -1));

        jLabel7.setText("Availability:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 300, -1, -1));
        add(drugPricejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 410, 160, -1));
        add(availabilityjTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 300, 160, -1));

        jLabel9.setText("Drug Price");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 410, -1, -1));
        add(manufacturerjTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 350, 160, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed

        //txtId.setEditable(true);
        txtName.setEditable(true);
        expiryDatetxt.setEditable(true);
        btnSave.setEnabled(true);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        backAction();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
      drug.setDrugName(drugPricejTextField.getText());
        drug.setManufactureDate(manufactureDate.getText());
        drug.setExpiryDate(expiryDatetxt.getText());
        drug.setSerialNo(Integer.parseInt(serialNotxt.getText()));
        
        drug.setAvailability(Integer.parseInt(availabilityjTextField1.getText()));
        drug.setManufacturerName(manufacturerjTextField1.getText());
        drug.setDrugPrice(Float.parseFloat(drugPricejTextField.getText()));
    }//GEN-LAST:event_btnSaveActionPerformed

      private void backAction() {
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField availabilityjTextField1;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JTextField drugPricejTextField;
    private javax.swing.JTextField expiryDatetxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField manufactureDate;
    private javax.swing.JTextField manufacturerjTextField1;
    private javax.swing.JTextField serialNotxt;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
