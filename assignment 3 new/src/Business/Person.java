/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Prashant
 */
public class Person {
   String personName;
   int personAge;
   
   private VitalSignHistory vitalSignHistory;
   
    
   public Person()
    {
        this.vitalSignHistory = new VitalSignHistory();
    }

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }
  

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String PersonName) {
        this.personName = PersonName;
    }

    

    @Override
    public String toString() {
        return  personName ;
    }
    
}
