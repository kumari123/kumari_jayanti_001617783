/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Child.Child;
import Business.Config.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.Organization.SchoolDoctorOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.SchoolDocRole.SchoolDoctorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Prashant
 */
public class SchoolDoctorRole extends Role{

    

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business, EnterpriseDirectory enterpriseDirectory) {
       return new SchoolDoctorWorkAreaJPanel(userProcessContainer, account, (SchoolDoctorOrganization) organization, enterprise,enterpriseDirectory);
    }
    
}
