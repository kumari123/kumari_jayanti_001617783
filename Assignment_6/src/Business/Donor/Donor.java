/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import Business.Employee.Employee;
import Business.Employee.EmployeeDirectory;

/**
 *
 * @author Prashant
 */
public class Donor extends Employee{
    
    
    private int donorAge;
    private EmployeeDirectory empdir;
    
    
    public Donor()
    {
       empdir=new EmployeeDirectory();
    }
    
   
    public int getDonorAge() {
        return donorAge;
    }

    public void setDonorAge(int donorAge) {
        this.donorAge = donorAge;
    }

    

    
}
