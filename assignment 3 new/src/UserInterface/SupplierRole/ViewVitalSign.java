package UserInterface.SupplierRole;

import Business.VitalSign;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ViewVitalSign extends javax.swing.JPanel {

    JPanel userProcessContainer;
    private VitalSign vitalSign;
    ManageVitalSign managVitalSign;
    public ViewVitalSign(JPanel upc,VitalSign vs,ManageVitalSign manageVS) {
        initComponents();
       userProcessContainer=upc;
       vitalSign=vs;
       managVitalSign=manageVS;
       
        txtId.setText(convertInteger(vs.getPatientId()));
        patientNamejTextField.setText(vs.getPatientName());
      // agejTextField.setText(String.valueOf(vs.getPatientAge()));
       respRatetxt.setText(String.valueOf(vs.getRespiratoryRate()));
       txtheart.setText(String.valueOf(vs.getHeartRate()));
       sysbpjTextField.setText(String.valueOf(vs.getSystolicBloodPressure()));
       wtjTextField.setText(String.valueOf(vs.getWeight()));
       System.out.println("age" +vs.getPatientAge() ); 
    }

    public static String convertInteger(int i) {
        return Integer.toString(i);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnUpdate = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtheart = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        respRatetxt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        sysbpjTextField = new javax.swing.JTextField();
        wtjTextField = new javax.swing.JTextField();
        patientNamejTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 102, 51));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setText("Update Product");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 490, 176, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setText("SAVE");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 490, 70, 30));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("VIEW VITALSIGN");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Respiratory Rate");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, 140, 30));

        txtId.setEditable(false);
        txtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 210, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Heart Rate");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, 110, 30));

        txtheart.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtheart, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 270, 210, 30));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Patient Name");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, -1, 30));

        respRatetxt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(respRatetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 220, 210, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Weight");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("Sysbolic Blood Pressure");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, -1));
        add(sysbpjTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 330, 210, 30));
        add(wtjTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 380, 210, 30));
        add(patientNamejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 170, 210, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Patient Id");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        btnSave.setEnabled(true);
        patientNamejTextField.setEditable(true);
       // agejTextField.setEditable(true);
        respRatetxt.setEditable(true);
        txtheart.setEditable(true);
        sysbpjTextField.setEditable(true);
        wtjTextField.setEditable(true);
}//GEN-LAST:event_btnUpdateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
       managVitalSign.populateTable();
        
        userProcessContainer.remove(this);
      Component [] componentArray=userProcessContainer.getComponents();
       Component component=componentArray[componentArray.length -1];
      ManageVitalSign managevs = (ManageVitalSign) component;//component array is of component type and we need this to bcome
                                                                 //of type managesupplier as we have to refresh table present in managesupplier
       
      managevs.populateTable();
       
       CardLayout layout= (CardLayout) userProcessContainer.getLayout();
       layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
         vitalSign.setPatientName(patientNamejTextField.getText());
         //vitalSign.setPatientAge(Integer.parseInt(agejTextField.getText()));
         vitalSign.setRespiratoryRate(Float.parseFloat(respRatetxt.getText()));
         vitalSign.setHeartRate(Float.parseFloat(txtheart.getText()));
         vitalSign.setSystolicBloodPressure(Float.parseFloat(sysbpjTextField.getText()));
         vitalSign.setWeight(Float.parseFloat(wtjTextField.getText()));
    }//GEN-LAST:event_btnSaveActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField patientNamejTextField;
    private javax.swing.JTextField respRatetxt;
    private javax.swing.JTextField sysbpjTextField;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtheart;
    private javax.swing.JTextField wtjTextField;
    // End of variables declaration//GEN-END:variables
}
