/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import java.util.ArrayList;

/**
 *
 * @author Prashant
 */
public class DonorDirectory {
    private ArrayList<Donor> donorlist;
    
    public DonorDirectory()
    {
    donorlist=new ArrayList<>();
    }

    public ArrayList<Donor> getDonorlist() {
        return donorlist;
    }

    public void setDonorlist(ArrayList<Donor> donorlist) {
        this.donorlist = donorlist;
    }
    
    public Donor addDonor()
    {
        Donor d=new Donor();
        donorlist.add(d);
        return d;
    }
    
    public void deleteDonor(Donor d){
        donorlist.remove(d);
    }
            
    public Donor searchDonor(int id)
    {
       for(Donor d:donorlist)
       {
           if(d.getDonorId()==id)
               return d;
       }
       return null;
    }
    
}
