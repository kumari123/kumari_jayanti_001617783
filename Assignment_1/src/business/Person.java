/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Prashant
 */
public class Person 
{
  private String firstName;
  private String lastName;
  private String middleName;
  private Date dateOfBirth;
  private String streetAddress;
  private String town;
  private int zipCode;
  private String occupation;
  private String emailId;
  private int areaCodeOfPhoneNumber;
  private long phoneNumber;
  
  

 public void setFirstName(String fname)
{
 this.firstName=fname;
}
  
 public String getFirstName()
 {
 return firstName;
 }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    
    public String getDateOfBirth() {
        SimpleDateFormat datebirth= new SimpleDateFormat("yyyy/mm/dd");
        
        return datebirth.format(dateOfBirth);
    }

    /**
     *
     * @param date
     */
    public void setDateOfBirth(String date) throws ParseException {
        SimpleDateFormat datebirth= new SimpleDateFormat("yyyy/mm/dd");
        this.dateOfBirth = datebirth.parse(date);
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public int getAreaCodeOfPhoneNumber() {
        return areaCodeOfPhoneNumber;
    }

    public void setAreaCodeOfPhoneNumber(int areaCodeOfPhoneNumber) {
        this.areaCodeOfPhoneNumber = areaCodeOfPhoneNumber;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    
    
   
 
}