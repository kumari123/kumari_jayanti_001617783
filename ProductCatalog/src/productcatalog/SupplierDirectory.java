/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productcatalog;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 *
 * @author Prashant
 */
public class SupplierDirectory {
    private ArrayList<Suppliers> SupplierList;
    
     public SupplierDirectory()
     {
         SupplierList =new ArrayList<>();
     }

    public ArrayList<Suppliers> getSupplierList() {
        return SupplierList;
    }

    public void setSupplierList(ArrayList<Suppliers> SupplierList) {
        this.SupplierList = SupplierList;
    }
    
    public Suppliers addSuppliers()
    {
       Suppliers sd =new Suppliers(); 
        this.SupplierList.add(sd);
       return sd;
    }
    
}
