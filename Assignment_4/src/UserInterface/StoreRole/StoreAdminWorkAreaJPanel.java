package UserInterface.StoreRole;

import Business.OrderCatalog;
import Business.Store;
import Business.StoreCatalog;
import Business.Supplier;
import Business.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class StoreAdminWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
   // private SupplierDirectory supplierDirectory;
    private StoreCatalog storeCatalog;
   // private Store store;
    private OrderCatalog orderCatalog;
    public StoreAdminWorkAreaJPanel(JPanel upc,StoreCatalog storeCat,OrderCatalog orderCatalog) {   //SupplierDirectory supplierDir,
        initComponents();
        userProcessContainer = upc;
       // supplierDirectory = supplierDir;
        storeCatalog=storeCat;
        this.orderCatalog=orderCatalog;
       // this.store=store;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        storeLogin = new javax.swing.JButton();
        supplierlogin = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Store Admin Work Area");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, -1, -1));

        storeLogin.setText("Store Login");
        storeLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storeLoginActionPerformed(evt);
            }
        });
        add(storeLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 130, -1, -1));

        supplierlogin.setText("Supplier");
        supplierlogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierloginActionPerformed(evt);
            }
        });
        add(supplierlogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 220, 90, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void supplierloginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierloginActionPerformed
    SelectSupplier ms = new SelectSupplier(userProcessContainer, storeCatalog,orderCatalog);
    userProcessContainer.add("SelectSupplier", ms);
    CardLayout layout = (CardLayout) userProcessContainer.getLayout();
    layout.next(userProcessContainer); // TODO add your handling code here:
    }//GEN-LAST:event_supplierloginActionPerformed

    private void storeLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storeLoginActionPerformed
    LoginStore ms = new LoginStore(userProcessContainer, storeCatalog,orderCatalog);
    userProcessContainer.add("LoginStore", ms);
    CardLayout layout = (CardLayout) userProcessContainer.getLayout();
    layout.next(userProcessContainer);
    // TODO add your handling code here:
    }//GEN-LAST:event_storeLoginActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton storeLogin;
    private javax.swing.JButton supplierlogin;
    // End of variables declaration//GEN-END:variables
}
