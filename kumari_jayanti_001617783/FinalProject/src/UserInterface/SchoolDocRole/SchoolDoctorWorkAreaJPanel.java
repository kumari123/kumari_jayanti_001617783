/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SchoolDocRole;

import Business.Child.Child;
import Business.Child.ChildDirectory;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.SchoolDoctorOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.AssignmentWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Prashant
 */
public class SchoolDoctorWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SchoolDoctorWorkAreaJPanel
     */
     private JPanel userProcessContainer;
    private SchoolDoctorOrganization organization;
     private Child child;
    private ChildDirectory childDirectory;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private EnterpriseDirectory entDirectory;
    ArrayList<ChildDirectory> childList;
 
    public SchoolDoctorWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, SchoolDoctorOrganization organization, Enterprise enterprise,EnterpriseDirectory entDirectory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.entDirectory=entDirectory;
        childList = new ArrayList<ChildDirectory>();
        jLabel1.setText(enterprise.getName());
        populateChild();
//        populateRequestTable();
        populateReqTable();
//        populateBankReqTable();
        
    }
    
    
     public void populateChild(){
        DefaultTableModel dtm = (DefaultTableModel) showChild.getModel();
        dtm.setRowCount(0);
        for(Child c: enterprise.getChildDirectory().getChildlist()){
            
                Object row[] = new Object[8];
                row[0] = c;
                row[1] = c.getName();
                row[2] = c.getAdmissionDate();
                row[3]=c.getAge();
                row[4]=c.getMotherName();
                row[5]=c.getFatherName();
                row[6]=c.getAddress();
                row[7]= enterprise.getChildDirectory().getDisability();
                
                        dtm.addRow(row);
            }
            
        }
     
    
//    public void populateRequestTable(){
//        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();
//        
//        model.setRowCount(0);
////        for(Enterprise e:entDirectory.getEnterpriseList()){
//       for(UserAccount a:enterprise.getUserAccountDirectory().getUserAccountList()){
//        for (WorkRequest request : a.getWorkQueue().getWorkRequestList()){
//            Object[] row = new Object[4];
//            row[0] = request.getMessage();
//            row[1] = request.getReceiver();
//            row[2] = request.getStatus();
//            String result = ((AssignmentWorkRequest) request).getAssignment();
//            row[3] = result == null ? "Waiting" : result;
//            
//            model.addRow(row);
//        }
//        }
//   }
//    }
    
    
     public void populateReqTable(){
        DefaultTableModel model = (DefaultTableModel) workReqJTable.getModel();
        
        model.setRowCount(0);
//        for(Enterprise e:entDirectory.getEnterpriseList()){
     for(UserAccount a:enterprise.getUserAccountDirectory().getUserAccountList()){
        for (WorkRequest request : a.getWorkQueue().getWorkRequestList()){
            Object[] row = new Object[5];
            row[0] = request.getMessage();
            row[1] = request.getReceiver();
            row[2] = request.getStatus();
            String result = ((AssignmentWorkRequest) request).getAssignment();
            row[3] = result == null ? "Waiting" : result;
            String report=((AssignmentWorkRequest)request).getReport();
            row[4]= report== null? "   ":report;
            
            model.addRow(row);
        }
        }
  }
//     }
     
//     public void populateBankReqTable(){
//        DefaultTableModel model = (DefaultTableModel) bankPersonjTable.getModel();
//        
//        model.setRowCount(0);
////        for(Enterprise e:entDirectory.getEnterpriseList()){
//       for(UserAccount a:enterprise.getUserAccountDirectory().getUserAccountList()){
//        { 
//        for (WorkRequest request : a.getWorkQueue().getWorkRequestList()){
//            Object[] row = new Object[4];
//            row[0] = request.getMessage();
//            row[1] = request.getReceiver();
//            row[2] = request.getStatus();
//            String result = ((AssignmentWorkRequest) request).getAssignment();
//            row[3] = result == null ? "Waiting" : result;
//            
//            model.addRow(row);
//        }
//        }}
//   }
//     }
    
    
       
  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        requestTeacherJButton = new javax.swing.JButton();
        refreshTestJButton = new javax.swing.JButton();
        enterpriseLabel = new javax.swing.JLabel();
        requestCaregiverJButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        workReqJTable = new javax.swing.JTable();
        bankPersonjButton = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        showChild = new javax.swing.JTable();

        requestTeacherJButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        requestTeacherJButton.setText("REQUEST TEACHER");
        requestTeacherJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTeacherJButtonActionPerformed(evt);
            }
        });

        refreshTestJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/REFRESH.png"))); // NOI18N
        refreshTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshTestJButtonActionPerformed(evt);
            }
        });

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enterpriseLabel.setText("ENTERPRISE");

        requestCaregiverJButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        requestCaregiverJButton1.setText("REQUEST CAREGIVER");
        requestCaregiverJButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestCaregiverJButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("jLabel1");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("DOCTOR WORK AREA");

        workReqJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "MESSAGE", "RECEIVER", "STATUS", "RESULT", "REPORT OF CHILD"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(workReqJTable);
        if (workReqJTable.getColumnModel().getColumnCount() > 0) {
            workReqJTable.getColumnModel().getColumn(0).setResizable(false);
            workReqJTable.getColumnModel().getColumn(1).setResizable(false);
            workReqJTable.getColumnModel().getColumn(2).setResizable(false);
            workReqJTable.getColumnModel().getColumn(3).setResizable(false);
        }

        bankPersonjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bankPersonjButton.setText("REQUEST BANK PERSONAL");
        bankPersonjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bankPersonjButtonActionPerformed(evt);
            }
        });

        showChild.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CHILD ID", "CHILD NAME", "CHILD ENROLLMENT DATE", "AGE", "MOTHER'S NAME", "FATHER'S NAME", "ADDRESS", "DISABILITY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(showChild);
        if (showChild.getColumnModel().getColumnCount() > 0) {
            showChild.getColumnModel().getColumn(0).setResizable(false);
            showChild.getColumnModel().getColumn(1).setResizable(false);
            showChild.getColumnModel().getColumn(2).setResizable(false);
            showChild.getColumnModel().getColumn(3).setResizable(false);
            showChild.getColumnModel().getColumn(4).setResizable(false);
            showChild.getColumnModel().getColumn(5).setResizable(false);
            showChild.getColumnModel().getColumn(6).setResizable(false);
            showChild.getColumnModel().getColumn(7).setResizable(false);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(384, 384, 384)
                        .addComponent(jLabel2)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(refreshTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 53, Short.MAX_VALUE)
                .addComponent(requestTeacherJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(requestCaregiverJButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(bankPersonjButton)
                .addGap(232, 232, 232))
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 961, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 961, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(refreshTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestTeacherJButton)
                    .addComponent(requestCaregiverJButton1)
                    .addComponent(bankPersonjButton))
                .addGap(135, 135, 135)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(206, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void requestTeacherJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTeacherJButtonActionPerformed
 int row = showChild.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(this, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
        Child child = (Child)showChild.getValueAt(row, 0);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new RequestTeacherJPanel(userProcessContainer, userAccount, enterprise,entDirectory,child));
        layout.next(userProcessContainer);

    }//GEN-LAST:event_requestTeacherJButtonActionPerformed
    }
    private void refreshTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshTestJButtonActionPerformed

        populateChild();
//        populateRequestTable();
        populateReqTable();
//        populateBankReqTable();


    }//GEN-LAST:event_refreshTestJButtonActionPerformed

    private void requestCaregiverJButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestCaregiverJButton1ActionPerformed
      
         int row = showChild.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(this, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
        Child child = (Child)showChild.getValueAt(row, 0);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new RequestCaregiverJPanel(userProcessContainer, userAccount, enterprise,entDirectory,child));
        layout.next(userProcessContainer);
        // TODO add your handling code here:
    }//GEN-LAST:event_requestCaregiverJButton1ActionPerformed
    }
    private void bankPersonjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bankPersonjButtonActionPerformed
      
          int row = showChild.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(this, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
        Child child = (Child)showChild.getValueAt(row, 0);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("RequestLabTestJPanel", new RequestBankPersonalJPanel(userProcessContainer, userAccount, enterprise,entDirectory,child));
        layout.next(userProcessContainer);// TODO add your handling code here:
    }//GEN-LAST:event_bankPersonjButtonActionPerformed
    }    
  
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bankPersonjButton;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JButton refreshTestJButton;
    private javax.swing.JButton requestCaregiverJButton1;
    private javax.swing.JButton requestTeacherJButton;
    private javax.swing.JTable showChild;
    private javax.swing.JTable workReqJTable;
    // End of variables declaration//GEN-END:variables
}
