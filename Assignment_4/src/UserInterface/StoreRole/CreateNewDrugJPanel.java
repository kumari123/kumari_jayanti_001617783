package UserInterface.StoreRole;

import Business.Drug;
import Business.Supplier;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class CreateNewDrugJPanel extends javax.swing.JPanel {

    JPanel userProcessContainer;
    Supplier supplier;
    public CreateNewDrugJPanel(JPanel upc, Supplier s){
        initComponents();
        userProcessContainer = upc;
        supplier = s;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        serialnojTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        manufacturerjTextField = new javax.swing.JTextField();
        availabilityjTextField1 = new javax.swing.JTextField();
        expiryDatejTextField = new javax.swing.JTextField();
        manufactureDatejTextField = new javax.swing.JTextField();
        drugnamejTextField = new javax.swing.JTextField();
        drugPricejTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Create New Drug");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setText("Add Product");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 510, -1, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 510, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Drug Name:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Expiry Date:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 30));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Manufacture Date:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, 30));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Serial No:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));
        add(serialnojTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, 160, -1));

        jLabel6.setText("Manufacturer Name:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 350, -1, -1));

        jLabel7.setText("Availability:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 300, -1, -1));
        add(manufacturerjTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 350, 160, -1));
        add(availabilityjTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 300, 160, -1));
        add(expiryDatejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 170, 160, -1));
        add(manufactureDatejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 140, 160, -1));
        add(drugnamejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 100, 160, -1));
        add(drugPricejTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 410, 160, -1));

        jLabel9.setText("Drug Price");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 410, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        Drug d = supplier.getDrugCatalog().addDrug();
        d.setDrugName(drugnamejTextField.getText());
        d.setManufactureDate(manufactureDatejTextField.getText());
        d.setExpiryDate(expiryDatejTextField.getText());
        d.setSerialNo(Integer.parseInt(serialnojTextField.getText()));
        
        d.setAvailability(Integer.parseInt(availabilityjTextField1.getText()));
        d.setManufacturerName(manufacturerjTextField.getText());
        d.setDrugPrice(Float.parseFloat(drugPricejTextField.getText()));
        JOptionPane.showMessageDialog(null, "Drug added!", "Info", JOptionPane.INFORMATION_MESSAGE);
}//GEN-LAST:event_btnAddActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageDrugCatalogJPanel manageProductCatalogJPanel = (ManageDrugCatalogJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField availabilityjTextField1;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JTextField drugPricejTextField;
    private javax.swing.JTextField drugnamejTextField;
    private javax.swing.JTextField expiryDatejTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField manufactureDatejTextField;
    private javax.swing.JTextField manufacturerjTextField;
    private javax.swing.JTextField serialnojTextField;
    // End of variables declaration//GEN-END:variables
}
