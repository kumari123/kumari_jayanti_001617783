/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;


import Business.BankPersonal.BankPersonWorkAreaJPanel;
import Business.Config.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Prashant
 */
public class BankRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business, EnterpriseDirectory enterpriseDirectory) {
     return new BankPersonWorkAreaJPanel(userProcessContainer, account, organization, enterprise,enterpriseDirectory);
    }
    
}
