/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Patient;
import Business.VitalSign;
import Business.VitalSignHistory;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Prashant
 */
public class VitalSignInfJPanel extends javax.swing.JPanel {

    /**
     * Creates new form VitalSignInfJPanel
     */
    private Patient patient;
    private VitalSignHistory vitalSignHistory;
    private VitalSign vitalSign;
    public VitalSignInfJPanel(Patient patient) {
        initComponents();
        this.patient=patient;
        this.vitalSignHistory=vitalSignHistory;
         vitalSign = new VitalSign();
        
    }

   

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        wtJTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        saveJButton = new javax.swing.JButton();
        sysbpJTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        heartJTextField = new javax.swing.JTextField();
        respiratoryJTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("SYSTOLIC BLOOD PRESSURE");

        wtJTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wtJTextFieldActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("HEART RATE");

        saveJButton.setText("SAVE");
        saveJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveJButtonActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("RESPIRATORY RATE");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("VITAL SIGN INFORMATION");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("WEIGHT(pounds)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addGap(58, 58, 58)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(sysbpJTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .addComponent(heartJTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(respiratoryJTextField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(wtJTextField)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(269, 269, 269)
                        .addComponent(saveJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(229, 229, 229)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(278, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel13)
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(respiratoryJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(heartJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(sysbpJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(wtJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(112, 112, 112)
                .addComponent(saveJButton)
                .addContainerGap(196, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void wtJTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wtJTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_wtJTextFieldActionPerformed

    private void saveJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveJButtonActionPerformed
        if(!check())
        {
            JOptionPane.showMessageDialog(null,"Fill up empty fields");
            return;
        }
       
       try
       {vitalSign.setRespiratoryRate(Float.parseFloat(respiratoryJTextField.getText()));}
       catch(NumberFormatException a)
       {
          JOptionPane.showMessageDialog(this,"Input only Integers for the Respiratory Rate","Warning",JOptionPane.WARNING_MESSAGE);
          return; 
       }
       
       try
       { vitalSign.setHeartRate(Float.parseFloat(heartJTextField.getText()));}
       catch(NumberFormatException b)
       {
          JOptionPane.showMessageDialog(this,"Input only Integers for the Respiratory Rate","Warning",JOptionPane.WARNING_MESSAGE);
          return; 
       } 
       
       try
       {vitalSign.setSystolicBloodPressure(Float.parseFloat(sysbpJTextField.getText())); }
        catch(NumberFormatException c)
       {
          JOptionPane.showMessageDialog(this,"Input only Integers for the SystolicBloodPressure","Warning",JOptionPane.WARNING_MESSAGE);
          return; 
       } 
        
       try 
       {vitalSign.setWeight(Float.parseFloat(wtJTextField.getText()));}
        catch(NumberFormatException d)
       {
          JOptionPane.showMessageDialog(this,"Input only Integers for the Weight","Warning",JOptionPane.WARNING_MESSAGE);
          return; 
       } 
        
       
       

       VitalSign vitalSign=this.patient.getVitalSignHistory().addVitalSign();
       vitalSign.setRespiratoryRate(Float.parseFloat(respiratoryJTextField.getText()));
       vitalSign.setHeartRate(Float.parseFloat(heartJTextField.getText())); 
       vitalSign.setSystolicBloodPressure(Float.parseFloat(sysbpJTextField.getText())); 
        vitalSign.setWeight(Float.parseFloat(wtJTextField.getText()));
      

        JOptionPane.showMessageDialog(null,"VitalSign is successfully added","Information",JOptionPane.INFORMATION_MESSAGE);
        resetFields();

        // TODO add your handling code here:
    }//GEN-LAST:event_saveJButtonActionPerformed
    
     private boolean check()
    {
     
        if(this.respiratoryJTextField.getText().equals("")||this.heartJTextField.getText().equals("")||this.sysbpJTextField.getText().equals("")||
                this.wtJTextField.getText().equals(""))
            return false;
        else
            return true ;
        
    }
    
    public void resetFields()
     {
        respiratoryJTextField.setText("");
        heartJTextField.setText("");
        sysbpJTextField.setText("");
        wtJTextField.setText("");
        
     }  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField heartJTextField;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField respiratoryJTextField;
    private javax.swing.JButton saveJButton;
    private javax.swing.JTextField sysbpJTextField;
    private javax.swing.JTextField wtJTextField;
    // End of variables declaration//GEN-END:variables

   
}
